# Badger

This repo contains a file called "badger.js" written in Node.js.  Its main function is to generate information about user "streaks", consecutive days of observations.

### Repository structure

There are two files of import in this repo:

- badger.js: contains the main "streak" function
- protocols.txt: contains a list of GLOBE protocols, with an "X" marking those appropriate for this function

### Requirements

This function is dependent on the fs-extra npm module for making web requests.  It may be installed from the Terminal as follows:

```buildoutcfg
$ npm install fs-extra
```

### Main function overview

The "streak" function accepts a user identification number, and a protocol name.  It calls the GLOBE API with a date range spanning 4/22/2010 to the present date, and collects all observations made by the user and adjusts all times to approximately local time using the longitude of the observation.  Then it extracts all sequences greater than one consecutive day.  It pulls out the longest streak, and also a current streak if there is one.  If there is no current streak, it begins a one day streak starting today.

For example, after entering a Node.js session at the Terminal prompt and loading the file as follows:

```buildoutcfg
$ node
> .load badger.js
```

Run the streak function for user id 51046601, looking for streaks in the mosquito habitat mapper protocol:

```buildoutcfg
> streak(51046601, 'mosquito_habitat_mapper')
```

### Example of data returned

Here is an example of the streak data returned:

```
{
  user: 51046601,
  current: { length: 1, beginning: '2020-09-24', dates: [ '2020-09-24' ] },
  longest: {
    length: 18,
    beginning: '2019-03-18',
    dates: [
      '2019-03-18', '2019-03-19',
      '2019-03-20', '2019-03-21',
      '2019-03-22', '2019-03-23',
      '2019-03-24', '2019-03-25',
      '2019-03-26', '2019-03-27',
      '2019-03-28', '2019-03-29',
      '2019-03-30', '2019-03-31',
      '2019-04-01', '2019-04-02',
      '2019-04-03', '2019-04-04'
    ]
  },
  all: [
    { length: 18, beginning: '2019-03-18', dates: [Array] },
    { length: 17, beginning: '2019-09-29', dates: [Array] },
    { length: 4, beginning: '2020-02-26', dates: [Array] },
    { length: 4, beginning: '2019-11-01', dates: [Array] },
    { length: 4, beginning: '2019-04-15', dates: [Array] },
    { length: 3, beginning: '2020-01-24', dates: [Array] },
    { length: 3, beginning: '2019-11-16', dates: [Array] },
    { length: 3, beginning: '2019-10-18', dates: [Array] },
    { length: 3, beginning: '2019-09-20', dates: [Array] },
    { length: 3, beginning: '2019-08-21', dates: [Array] },
    { length: 3, beginning: '2019-04-11', dates: [Array] },
    { length: 3, beginning: '2019-04-07', dates: [Array] },
    { length: 3, beginning: '2019-03-06', dates: [Array] },
    { length: 2, beginning: '2020-04-07', dates: [Array] },
    { length: 2, beginning: '2020-03-23', dates: [Array] },
    { length: 2, beginning: '2019-11-08', dates: [Array] },
    { length: 2, beginning: '2019-10-27', dates: [Array] },
    { length: 2, beginning: '2019-10-22', dates: [Array] },
    { length: 2, beginning: '2019-09-17', dates: [Array] },
    { length: 2, beginning: '2019-08-02', dates: [Array] },
    { length: 2, beginning: '2019-05-15', dates: [Array] },
    { length: 2, beginning: '2019-03-10', dates: [Array] },
    { length: 2, beginning: '2019-02-14', dates: [Array] },
    { length: 2, beginning: '2018-12-15', dates: [Array] }
  ]
}
```

#### Thank you!

- Matthew Bandel, SSAI, Senior Scientific Programmer