// badger.js code to search users for streaks

// get file system
var fs = require('fs-extra');

// get http
var https = require('https');


// bundle up the list of associated protocols
function bundle() {

    // parse list of protocols from file
    protocols = fs.readFileSync('protocols.txt', 'utf-8');
    protocols = protocols.split('\n');

    // only keep those with out an X
    protocols = protocols.filter(function(protocol) {return protocol.startsWith('X')});
    protocols = protocols.map(function(protocol) {return protocol.replace('X ', '')});

    return protocols;
}


// call the api with a protocol and user id
function call(protocol, user) {

    // assemble the url for the API call
    var url = 'https://api.globe.gov/search/v1/measurement/protocol/measureddate/userid/';
    url += '?protocols=' + protocol;
    url += '&startdate=' + '2010-04-22';
    url += '&enddate=' + new Date(Date.now()).toISOString().split('T')[0];
    url += '&userid=' + user;
    url += '&geojson=FALSE';
    url += '&sample=FALSE';

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // make the api call
        console.log('requesting ' + url + '...');
        var request = https.get(url, function(response) {

            // initialize data
            var data = '';

            // accumulate chunks of data
            response.on('data', function(chunk) {

                // accumulate data
                data += chunk;
            })
            // The whole response has been received. Print out the result.
            response.on('end', function() {

                // resolve as data
                data = JSON.parse(data);
                resolve(data['results']);
            })
        })
        // check for errors
        request.on("error", function(error) {

            // log error
            console.log("Error: " + error.message);
            reject([]);
        })
    })
    return promise;
}


// find coincident records with mosquito dates
function coincide(user) {

    // get user records
    var crossreferencing = crossreference(user);
    crossreferencing.then(function(cross) {

        // get mosquitoes records
        var mosquitoes = 'mosquito_habitat_mapper';
        var observations = cross[mosquitoes];

        // get all unique dates
        var measured = 'measuredDate';
        var dates = observations.map(function(record) {return record[measured]});
        dates = [...new Set(dates)];

        // get all keys from report
        var keys = Object.keys(cross);
        keys = keys.filter(function(key) {return key != mosquitoes});

        // get summaries
        var summaries = [];
        keys.forEach(function(key) {

            // get records
            var records = cross[key];
            if (records != undefined) {

                var length = records.length;
                var datesii = records.map(function(record) {return record[measured]});

                // get coincident dates
                datesii = datesii.filter(function(date) {return dates.includes(date)});
                var coincidences = datesii.length;

                // make summary
                var summary = {'protocol': key, 'length': length, 'coincidences': coincidences};
                summaries.push(summary);
            }
        })
        // sort summaries
        console.log('summary: ');
        summaries = summaries.filter(function(summary) {return summary.length > 0});
        summaries.sort(function(a, b) {return b.length - a.length});
        summaries.forEach(function(summary) {

            // print report
            console.log(summary.length + ' records from ' + summary.protocol + ', ' + summary.coincidences + ' on same date as mosquitoes');
        })
    })
    return;
}


// crossreference to get other protocols
function crossreference(user) {

    // begin reports
    var reports = {};

    // set mosquitoes protocol
    var mosquitoes = 'mosquito_habitat_mapper';

    // get protocols list
    var protocols = bundle();
    protocols.push(mosquitoes);

    // begin promise
    promise = new Promise(function(resolve, reject) {

        // begin promises
        var promises = [];

        // go through each protocol
        protocols.forEach(function(protocol) {

            // get mosquitoes records
            var calling = call(protocol, user);
            promises.push(calling);
            calling.then(function(records) {

                // add to reports
                reports[protocol] = records;

                // make call for protocol
                var crossreferencing = call(protocol, user);
                crossreferencing.then(function(records) {

                    // add to reports
                    reports[protocol] = records;
                })
            })
        })
        // resolve
        Promise.allSettled(promises).then(function() {

            // view all promises
            console.log(promises)

            // resolve
            resolve(reports);
        })
    })
    return promise;
}


// gather streak information about user id regarding mosquito habitat
function streak(user, protocol='mosquito_habitat_mapper') {

    // begin promise
    promise = new Promise(function(resolve, reject) {

        // get measured time field based on protocol
        var stub = [...protocol.split('_')].join('');
        var measured = stub + 'MeasuredAt';
        var measuredii = stub + 'MeasuredOn';

        // call api with user id
        var calling = call(protocol, user);
        calling.then(function(records) {

            // get dates from all records
            var dates = [];
            records.forEach(function(record) {

                // get time value
                if (Object.keys(record.data).includes(measured)) {

                    // get MeasuredAt field
                    var time = record.data[measured];
                }
                // otherwise
                else {

                    // try MeasuredOn
                    var time = record.data[measuredii];
                }
                // determine longitude adjustment
                var longitude = record['longitude'];
                var hours = Math.round(longitude * 12 / 180);

                // adjust date by hours
                var hour = 60 * 60 * 1000;
                time = new Date(new Date(time) - hours * hour);

                // make date
                var date = time.toISOString().split('T')[0];
                dates.push(date);
            })
            // get unique dates
            dates = [...new Set(dates)];
            dates.sort();

            // calculate number of milliseconds in a day
            var day = 24 * 60 * 60 * 1000;

            // keep dates only if before or after dates are also present
            dates = dates.map(function(date) {

                // assume token is 'GAP' for gaps greater than a day
                var token = 'GAP';

                // get before and after dates
                var before = new Date(new Date(date).valueOf() - day).toISOString().split('T')[0];
                var after = new Date(new Date(date).valueOf() + day).toISOString().split('T')[0];

                // check for before date
                if (dates.includes(before)) {

                    // set presence to true
                    token = date + 'GAP';
                }
                // check for after date
                if (dates.includes(after)) {

                    // set presence to true
                    token = 'GAP' + date;
                }
                // remove gaps if on both sides
                if (dates.includes(before) && dates.includes(after)) {

                    // remove GAPs
                    token = date;
                }
                return token;
            })
            // join all dates together and split along GAPs, discarding extra commas
            dates = dates.join();
            var splittings = dates.split('GAP');
            splittings = splittings.filter(function(splitting) {return splitting.length > 1});

            // get local time and adjust for local time (offset in minutes)
            var timezone = new Date().getTimezoneOffset();
            var minute = 60 * 1000;
            var now = new Date(Date.now() + timezone * minute);
            var today = now.toISOString().split('T')[0];

            // make default streak
            var current = {'length': 1, 'beginning': today, 'dates': [today]};

            // make streak objects
            var streaks = {'user': user, 'current': current, 'longest': current, 'all': []};
            splittings.forEach(function(splitting) {

                // get sequence
                var sequence = splitting.split(',');
                sequence = sequence.filter(function(member) {return member.length > 1});

                // if sequence is longer than one day
                if (sequence.length > 1) {

                    // make streak
                    var streak = {'length': sequence.length, 'beginning': sequence[0], 'dates': sequence};

                    // check for current streak
                    var last = sequence[sequence.length - 1];
                    if (new Date(today) - new Date(last) < day) {

                        // set to current
                        streak.current = streak;
                    }
                    // check for longest
                    if (streak.length > streaks.longest.length) {

                        // make longest
                        streaks.longest = streak;
                    }
                    // add to streaks
                    streaks.all.push(streak);
                }
            })
            // sort streaks
            streaks.all = streaks.all.sort(function(a, b) {return new Date(b['beginning']) - new Date(a['beginning'])});
            streaks.all = streaks.all.sort(function(a, b) {return b['length'] - a['length']});

            // write to file
            var deposit = 'streaks_' + user + '.json';
            fs.writeFileSync(deposit, JSON.stringify(streaks, '', 4));

            // view results
            console.log(protocol);
            console.log(measured, measuredii);

            // resolve promise
            console.log(streaks);
            resolve(streaks);
        })
    })
    return promise;
}


// print status
console.log('badger loaded.');
