# machanic.py to calibrate Jimp blur factors using support vector machines

# import reload
from importlib import reload

# import os and sys modules for system controls
import os
import sys

# import requests and json modules for making API requests
import json

# import numpy for math
from numpy import array, isnan
from numpy import exp, sqrt, log, log10, sign, abs
from numpy import arcsin, sin, cos, pi
from numpy import average, std, histogram, percentile
from numpy.random import random, choice

# import sci-kit for linear regressions
from sklearn.neighbors import BallTree
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVC

# import matplotlib
from matplotlib import pyplot


# calibrate function to calibrate blur degrees according to height
def calibrate(mode, jason, save=True):
    """Calibrate the blur regression line using SVM.

    Arguments:
        mode: str, the mode
        jason; str, json file name
        save: boolean, save files?

    Returns:
        None
    """

    # get training data
    with open(jason, 'r') as pointer:

        # get training data
        training = json.load(pointer)

    # construct matrices
    heights = [float(entry['height']) for entry in training.values()]
    degrees = [float(entry['degree']) for entry in training.values()]
    detections = [int(entry['detected']) for entry in training.values()]

    # perform SVM
    matrix = array([[x, y] for x, y in zip(heights, degrees)])
    targets = array(detections)

    # perform svm
    weights = {1: 0.9, 0: 0.1}
    support = SVC(kernel='linear', class_weight=weights).fit(matrix, targets)

    # make colors
    colors = {0: 'go', 1: 'ro'}

    # plot figure
    for height, degree, detection in zip(heights, degrees, detections):

        # plot point
        pyplot.plot(height, degree, colors[detection])

    # construct border
    left = min(heights)
    right = max(heights)
    bottom = min(degrees)
    top = max(degrees)

    # get 1000 horizontal and vertical points
    chunk = (right - left) / 1000
    horizontals = [left + chunk * index for index in range(1001)]
    chunk = (top - bottom) / 1000
    verticals = [bottom + chunk * index for index in range(1001)]

    # make border
    border = [(horizontal, bottom) for horizontal in horizontals]
    border += [(right, vertical) for vertical in verticals]
    border += [(horizontal, top) for horizontal in reversed(horizontals)]
    border += [(left, vertical) for vertical in reversed(verticals)]
    border += [(left, bottom)]

    # make predictions
    predictions = [support.predict([pair])[0] for pair in border]

    # zip together predictions with next prediction
    zipper = [(first, second) for first, second in zip(predictions[:-1], predictions[1:])]
    indices = [index for index, pair in enumerate(zipper) if pair[0] != pair[1]]

    # construct points
    xs = [average([border[index][0], border[index + 1][0]]) for index in indices]
    ys = [average([border[index][1], border[index + 1][1]]) for index in indices]

    # calculate slope and intercept
    slope = (ys[1] - ys[0]) / (xs[1] - xs[0])
    intercept = ys[0] - slope * xs[0]

    # plot dividing line
    pyplot.plot(xs, ys, 'k--')

    # save changes
    if save:

        # open calibrations file
        with open('calibrations.json', 'r') as pointer:

            # get calibrations
            calibrations = json.load(pointer)

        # update calibration
        calibrations[mode] = {'slope': slope, 'intercept': intercept}

        # open calibrations file
        with open('calibrations.json', 'w') as pointer:

            # get calibrations
            json.dump(calibrations, pointer)

    # label plot
    pyplot.title('Automatic calibration of blur degrees for {} \ny = {} x + {}'.format(mode, round(slope, 2), round(intercept, 2)))
    pyplot.xlabel('height')
    pyplot.ylabel('degree')

    # save changes
    if save:

        # save plot
        pyplot.savefig(mode + '_calibration.png')

    # save plot
    pyplot.show()
    pyplot.close()

    return None




# # make bokeh plot
# plot = figure(plot_height=300, plot_width=600, x_axis_label='height', y_axis_label='blur')
#
# # add legible markers
# xs = [float(entry.split('_')[-2]) for entry in legibles]
# ys = [float(entry.split('.')[0].split('_')[-1]) for entry in legibles]
# plot.circle(x=xs, y=ys, color='red', size=10)
#
# # add illegible markers
# xsii = [float(entry.split('_')[-2]) for entry in illegibles]
# ysii = [float(entry.split('.')[0].split('_')[-1]) for entry in illegibles]
# plot.circle(x=xsii, y=ysii, color='green', size=10)
#
# # get matrices
# matrix = array([[x, y] for x, y in zip(xs + xsii, ys + ysii)])
# targets = array([0] * len(xs) + [1] * len(xsii))
#
# # if member in each
# if len(legibles) > 0 and len(illegibles) > 0:
#
#     # calculate svm
#     support = SVC(kernel='linear').fit(matrix, targets)
#
#     # calculate regression line
#     matrixii = array([point[0] for point in support.support_vectors_]).reshape(-1, 1)
#     targetsii = array([point[1] for point in support.support_vectors_]).reshape(-1, 1)
#     regression = LinearRegression().fit(matrixii, targetsii)
#
#     # plot regression line
#     slope = regression.coef_[0][0]
#     intercept = regression.intercept_[0]
#     minimum = min(xs + xsii)
#     maximum = max(xs + xsii)
#     plot.line(x=[minimum, maximum], y=[slope * entry + intercept for entry in (minimum, maximum)], color='black')
#
#
#     # print line info
#     print('slope: {}, intercept: {}'.format(slope, intercept))
#
# # display
# output_notebook()
# show(plot)