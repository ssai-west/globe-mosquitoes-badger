# zippers.py to zip files together

# import zipfile
from zipfile import ZipFile

# function to zipfiles together
def zipper(title, *names):
    """Zip files together.

    Arguments:
        title: str, name of zip file
        *names, unpacked tuple of files to zip

    Returns:
        None
    """

    # begin album
    album = ZipFile(title, 'w')

    # add files
    for name in names:

        # add name
        album.write(name)

    return None
